package com.gitlab.mqus.gitlab;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.gitlab.mqus.gitlab.adapters.AbstractAdapter;
import com.gitlab.mqus.gitlab.adapters.Converter;
import com.gitlab.mqus.gitlab.output.GitlabCodeQualityItem;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.*;
import org.gradle.api.file.RegularFileProperty;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@SuppressWarnings("UnstableApiUsage")
@CacheableTask
public abstract class GitlabLintReportTask extends DefaultTask {

    private static final Logger logger = Logger.getLogger("io.github.mqus");

    private final List<Converter> converters;

    public GitlabLintReportTask() {
        converters = new ArrayList<>();
        getOutputFile().convention(getProject().getLayout().getBuildDirectory().file("reports/gitlab/quality.json"));
    }

    public void addAdapter(AbstractAdapter<?> adapter) {
        List<? extends Converter> converters = adapter.getConverters();
        this.converters.addAll(converters);
        converters.stream().map(Converter::getTasks).forEach(this::mustRunAfter);
    }

    @TaskAction
    public void convertLintReports() throws IOException {
        List<GitlabCodeQualityItem> issues = gatherIssues();
        writeIssues(issues);
    }

    @OutputFile
    public abstract RegularFileProperty getOutputFile();


    /**
     * A method purely for determining all the files used to generate the output.
     * This is useful for deriving caching properties and debugging, but should not be changed here.
     *
     * @return A set of all linter files that are converted to the output
     */
    @InputFiles
    @PathSensitive(PathSensitivity.RELATIVE)
    public Iterable<File> getInputFiles(){
        logger.fine("InputFiles queried...");
        List<File> output = converters.stream().flatMap(Converter::resolveSourceFiles).collect(Collectors.toList());
        if(logger.isLoggable(Level.FINE)){
            for (File file : output) {
                logger.fine("InputFile: "+file.getAbsolutePath());
            }
        }
        return output;
    }

    private List<GitlabCodeQualityItem> gatherIssues() {
        return converters.stream().flatMap(Converter::run).collect(Collectors.toList());
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressFBWarnings("RV_RETURN_VALUE_IGNORED_BAD_PRACTICE")
    private void writeIssues(List<GitlabCodeQualityItem> issues) throws IOException {
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);

        File target = getOutputFile().get().getAsFile();
        target.getParentFile().mkdirs();

        om.writeValue(target, issues);
    }
}
