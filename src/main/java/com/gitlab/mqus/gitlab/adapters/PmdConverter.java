package com.gitlab.mqus.gitlab.adapters;

import com.gitlab.mqus.gitlab.output.GitlabCodeQualityItem;
import com.gitlab.mqus.gitlab.output.Severity;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.plugins.quality.Pmd;
import org.gradle.api.tasks.TaskCollection;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static com.gitlab.mqus.gitlab.output.Severity.*;

public class PmdConverter implements Converter {
    private static Logger logger = Logger.getLogger(PmdConverter.class.getName());
    Project p;

    TaskCollection<Pmd> tasks;


    public static AbstractAdapter<PmdConverter> adapter(Project p){
        return new AbstractAdapter<PmdConverter>(p) {
            @Override
            protected PmdConverter converterFrom(Project project) {
                return new PmdConverter(project);
            }
        };
    }

    public PmdConverter(Project p) {
        this.p = p;
        this.tasks = p.getTasks().withType(Pmd.class);
    }


    @Override
    public Stream<GitlabCodeQualityItem> run() {
        return this.resolveSourceFiles()
                .filter(File::isFile)
                .flatMap(this::parsePmd);
    }

    @Override
    public Stream<File> resolveSourceFiles() {
        return tasks.stream()
                .flatMap(task -> task.getOutputs().getFiles().getFiles().stream())
                .distinct()
                .filter(f -> f.getName().endsWith(".xml"));
    }

    @Override
    public TaskCollection<? extends Task> getTasks() {
        return tasks;
    }

    private Stream<GitlabCodeQualityItem> parsePmd(File f) {
        try {
            return parsePmdInner(f);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new RuntimeException(e);
        }
    }

    private Stream<GitlabCodeQualityItem> parsePmdInner(File f) throws ParserConfigurationException, IOException, SAXException {
        // Example entry:
        //<file name="/home/mqus/proj/secretproject/src/main/java/com/company/Myclass.java">
        //<violation beginline="16" endline="16" begincolumn="2" endcolumn="42" rule="LinguisticNaming" ruleset="Code Style" package="com.company" class="Myclass" method="toJSON" externalInfoUrl="https://pmd.github.io/pmd-6.36.0/pmd_rules_java_codestyle.html#linguisticnaming" priority="3">
        //Linguistics Antipattern - The transform method 'toJSON' should not return void linguistically
        //</violation>
        //</file>
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(f);
        NodeList nodes = doc.getElementsByTagName("file");
        List<GitlabCodeQualityItem> issues = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node file = nodes.item(i);
            String absolutePath = file.getAttributes().getNamedItem("name").getNodeValue();
            for (int j = 0; j < file.getChildNodes().getLength(); j++) {
                Node violation = file.getChildNodes().item(j);
                if (!violation.getNodeName().equals("violation")) {
                    continue;
                }

                String extUrl = violation.getAttributes().getNamedItem("externalInfoUrl").getNodeValue();
                String description = violation.getTextContent().trim() + "\n\n" + extUrl;

                String rule = violation.getAttributes().getNamedItem("rule").getNodeValue();
                String ruleset = violation.getAttributes().getNamedItem("ruleset").getNodeValue();
                String checkId = "PMD:" + ruleset + "/" + rule;

                String prio = violation.getAttributes().getNamedItem("priority").getNodeValue();

                String path = Paths.get(p.getRootDir().getPath()).relativize(Paths.get(absolutePath)).toString();

                int line = Integer.parseInt(violation.getAttributes().getNamedItem("beginline").getNodeValue());

                String endline = violation.getAttributes().getNamedItem("endline").getNodeValue();
                String begincol = violation.getAttributes().getNamedItem("begincolumn").getNodeValue();
                String endcol = violation.getAttributes().getNamedItem("endcolumn").getNodeValue();
                String id = endline + "#" + begincol + "#" + endcol;

                logger.finer("Found issue: PMD: "+path+" \t"+checkId);
                issues.add(new GitlabCodeQualityItem(description, checkId, toSeverity(prio), path, line, id));

            }

        }
        return issues.stream();
    }

    private static Severity toSeverity(String sev) {
        switch (sev) {
            case "1":
                return BLOCKER;
            case "2":
                return CRITICAL;
            case "3":
                return MAJOR;
            case "4":
                return MINOR;
            case "5":
            default:
                return INFO;
        }
    }
}
