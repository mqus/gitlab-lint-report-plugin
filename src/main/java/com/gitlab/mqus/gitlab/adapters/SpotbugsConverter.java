package com.gitlab.mqus.gitlab.adapters;

import com.gitlab.mqus.gitlab.output.GitlabCodeQualityItem;
import com.gitlab.mqus.gitlab.output.Severity;
import com.github.spotbugs.snom.SpotBugsTask;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.tasks.TaskCollection;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static com.gitlab.mqus.gitlab.output.Severity.*;

public class SpotbugsConverter implements Converter {

    private static final Logger logger = Logger.getLogger(SpotbugsConverter.class.getName());

    Project p;

    TaskCollection<SpotBugsTask> tasks;


    public static AbstractAdapter<SpotbugsConverter> adapter(Project p){
        return new AbstractAdapter<SpotbugsConverter>(p) {
            @Override
            protected SpotbugsConverter converterFrom(Project project) {
                return new SpotbugsConverter(project);
            }
        };
    }

    public SpotbugsConverter(Project p) {
        this.p = p;
        this.tasks = p.getTasks().withType(SpotBugsTask.class);
    }


    @Override
    public Stream<GitlabCodeQualityItem> run() {
        return this.resolveSourceFiles()
                .filter(File::isFile)
                .flatMap(this::parseSpotbugs);
    }

    @Override
    public Stream<File> resolveSourceFiles() {
        return tasks.stream()
                .flatMap(task -> task.getOutputs().getFiles().getFiles().stream())
                .distinct()
                .filter(f->f.getName().endsWith(".xml"));
    }

    @Override
    public TaskCollection<? extends Task> getTasks() {
        return tasks;
    }

    private Stream<GitlabCodeQualityItem> parseSpotbugs(File f) {
        try {
            return parseSpotbugsInner(f);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new RuntimeException(e);
        }
    }

    private Stream<GitlabCodeQualityItem> parseSpotbugsInner(File f) throws ParserConfigurationException, IOException, SAXException {
        // Example entry:
        //  <BugInstance type="OS_OPEN_STREAM_EXCEPTION_PATH" priority="3" rank="19" abbrev="OS" category="BAD_PRACTICE" instanceHash="906e0995b63302d7c6efb4491f8489d0" instanceOccurrenceNum="0" instanceOccurrenceMax="0">
        //    <ShortMessage>Method may fail to close stream on exception</ShortMessage>
        //    <LongMessage>com.company.Someclass.writeBundle(String, String) may fail to close stream on exception</LongMessage>
        //    <Class classname="com.company.Someclass" primary="true">
        //      <SourceLine classname="com.company.Someclass" start="44" end="575" sourcefile="Editor.java" sourcepath="com/company/Someclass.java" relSourcepath="src/main/java/com/company/Someclass.java">
        //        <Message>At Editor.java:[lines 44-575]</Message>
        //      </SourceLine>
        //      <Message>In class com.company.Someclass</Message>
        //    </Class>
        //    <Method classname="com.company.Someclass" name="writeBundle" signature="(Ljava/lang/String;Ljava/lang/String;)V" isStatic="false" primary="true">
        //      <SourceLine classname="com.company.Someclass" start="388" end="461" startBytecode="0" endBytecode="271" sourcefile="Editor.java" sourcepath="com/company/Someclass.java" relSourcepath="src/main/java/com/company/Someclass.java"/>
        //      <Message>In method com.company.Someclass.writeBundle(String, String)</Message>
        //    </Method>
        //    <Type descriptor="Ljava/io/Writer;" role="TYPE_CLOSEIT">
        //      <SourceLine classname="java.io.Writer" start="51" end="390" sourcefile="Writer.java" sourcepath="java/io/Writer.java">
        //        <Message>At Writer.java:[lines 51-390]</Message>
        //      </SourceLine>
        //      <Message>Need to close java.io.Writer </Message>
        //    </Type>
        //    <SourceLine classname="com.company.Someclass" primary="true" start="398" end="398" startBytecode="34" endBytecode="34" sourcefile="Editor.java" sourcepath="com/company/Someclass.java" relSourcepath="src/main/java/com/company/Someclass.java">
        //      <Message>At Editor.java:[line 398]</Message>
        //    </SourceLine>
        //  </BugInstance>
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(f);
        NodeList nodes = doc.getElementsByTagName("BugCollection").item(0).getChildNodes();
        List<GitlabCodeQualityItem> issues = new ArrayList<>();
        List<String> srcDirs = new ArrayList<>();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node instance = nodes.item(i);
            if ( instance.getNodeName().equals("Project")) {
                srcDirs.addAll(getSourceDirectories(instance));
            }

            if (!instance.getNodeName().equals("BugInstance")) {
                continue;
            }

            String type = instance.getAttributes().getNamedItem("type").getNodeValue();
            String category = instance.getAttributes().getNamedItem("category").getNodeValue();
            String checkId = "Spotbugs:" + category + "/" + type;

            String id = instance.getAttributes().getNamedItem("instanceHash").getNodeValue();
            String prio = instance.getAttributes().getNamedItem("priority").getNodeValue();

            String path = "";
            int line = 0;
            String description = "";
            NodeList parts = instance.getChildNodes();
            for (int j = 0; j < parts.getLength(); j++) {
                Node part = parts.item(j);
                if (part.getNodeName().equals("SourceLine")) {
                    path = part.getAttributes().getNamedItem("relSourcepath").getNodeValue();
                    line = Integer.parseInt(part.getAttributes().getNamedItem("start").getNodeValue());
                } else if (part.getNodeName().equals("LongMessage")) {
                    description = part.getTextContent().trim();
                }

            }
            logger.finer("Found issue: Spotbugs: " + path + " \t" + checkId);

            issues.add(new GitlabCodeQualityItem(description, checkId, toSeverity(prio), path, line, id));

        }
        return issues.stream().map(new PathExtender(srcDirs));
    }

    private List<String> getSourceDirectories(Node instance) {
        NodeList nodes = instance.getChildNodes();
        List<String> output = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if(node.getNodeName().equals("SrcDir")){
                output.add(node.getTextContent().trim());
            }
        }
        return output;
    }

    private static Severity toSeverity(String sev) {
        switch (sev) {
            case "1":
                return BLOCKER;
            case "2":
                return CRITICAL;
            case "3":
                return MAJOR;
            case "4":
                return MINOR;
            case "5":
            default:
                return INFO;
        }
    }


    private class PathExtender implements Function<GitlabCodeQualityItem, GitlabCodeQualityItem> {
        private final Path rootPath;
        private final HashMap<String, Set<Path>> suffixMap;

        @SuppressFBWarnings("NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE")
        public PathExtender(List<String> srcDirs) {
            this.rootPath = p.getRootDir().toPath();
            this.suffixMap = new HashMap<>();
            for (String srcDir : srcDirs) {
                Path srcPath = Paths.get(srcDir).toAbsolutePath();
                String key = srcPath.getFileName().toString();
                suffixMap.computeIfAbsent(key, (unused)->new HashSet<>())
                        .add(srcPath.getParent());
            }
        }

        @Override
        public GitlabCodeQualityItem apply(GitlabCodeQualityItem item) {
            Path oldPath = Paths.get(item.getPath());
            Path newAbsPath = null;
            Set<Path> possibleLocations = this.suffixMap.get(oldPath.getName(0).toString());
            if(possibleLocations == null){
                possibleLocations = Collections.emptySet();
            }

            for (Path possibleLocation : possibleLocations) {
                Path testing = possibleLocation.resolve(oldPath);
                if(testing.toFile().isFile()){
                    newAbsPath = testing;
                    break;
                }
            }

            if(newAbsPath == null) {
                if (oldPath.toFile().isFile()) {
                    newAbsPath = oldPath.toAbsolutePath();
                }
            }

            if(newAbsPath != null){
                item.setPath(rootPath.relativize(newAbsPath).toString());
            } else {
                System.err.printf("Could not find file `%s` in following directories: %s%n", item.getPath(), possibleLocations);
            }

            return item;
        }
    }
}
