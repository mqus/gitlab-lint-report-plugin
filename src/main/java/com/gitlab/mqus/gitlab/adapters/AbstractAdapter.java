package com.gitlab.mqus.gitlab.adapters;

import org.gradle.api.Project;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractAdapter<T extends Converter> {
    Project rootProject;

    public AbstractAdapter(Project rootProject) {
        this.rootProject = rootProject;
    }

    public List<Converter> getConverters(){
        return rootProject.getAllprojects().stream().map(this::converterFrom).collect(Collectors.toList());
    }

    protected abstract T converterFrom(Project project);

}
