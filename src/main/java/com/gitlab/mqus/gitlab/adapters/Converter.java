package com.gitlab.mqus.gitlab.adapters;

import com.gitlab.mqus.gitlab.output.GitlabCodeQualityItem;
import org.gradle.api.Task;
import org.gradle.api.tasks.TaskCollection;

import java.io.File;
import java.util.stream.Stream;

public interface Converter {
    Stream<GitlabCodeQualityItem> run();

    Stream<File> resolveSourceFiles();

    TaskCollection<? extends Task> getTasks();
}
