package com.gitlab.mqus.gitlab.output;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.HashMap;
import java.util.Map;

@SuppressFBWarnings("URF_UNREAD_FIELD")
public class GitlabCodeQualityItem {
    @JsonProperty
    String description;
    @JsonProperty
    String check_name;
    @JsonIgnore
    Severity severity;

    @JsonIgnore
    String id;

    @JsonIgnore
    String path;
    @JsonIgnore
    int line;

    public GitlabCodeQualityItem(String description, String check_name, Severity severity, String path, int line) {
        this(description, check_name, severity, path, line, null);
    }

    public GitlabCodeQualityItem(String description, String check_name, Severity severity, String path, int line, String id) {
        this.description = description;
        this.check_name = check_name;
        this.severity = severity;
        this.path = path;
        this.line = line;
        this.id = id;
    }

    @JsonGetter
    public Map<String, Object> getLocation(){
        //produce a
        //    {
        //      "path": "lib/index.js",
        //      "lines": {
        //        "begin": 42
        //      }
        //    }
        // object for location

        Map<String,Object> outer = new HashMap<>();
        outer.put("path", path);
        Map<String,Integer> lines = new HashMap<>();
        lines.put("begin", line);
        outer.put("lines", lines);

        return outer;
    }

    @JsonGetter
    public String getSeverity(){
        return severity.name().toLowerCase();
    }

    @JsonGetter
    public String getFingerprint(){
        return this.path+"#"+this.line+"#"+this.check_name+"#"+this.id;
    }

    @JsonIgnore
    public String getPath() {
        return path;
    }

    @JsonIgnore
    public void setPath(String path) {
        this.path = path;
    }
}


