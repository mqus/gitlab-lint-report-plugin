package com.gitlab.mqus.gitlab;

import com.gitlab.mqus.gitlab.adapters.PmdConverter;
import com.gitlab.mqus.gitlab.adapters.SpotbugsConverter;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.tasks.TaskProvider;

@SuppressWarnings("UnstableApiUsage")
public class LintReportPlugin implements Plugin<Project> {


    @Override
    public void apply(Project project) {
        TaskProvider<GitlabLintReportTask> task = project.getTasks().register("convertLintReportsForGitLab", GitlabLintReportTask.class);
        task.configure(t ->{
            t.addAdapter(SpotbugsConverter.adapter(project));
            t.addAdapter(PmdConverter.adapter(project));
        });
    }
}
