
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.gradle.testkit.runner.TaskOutcome;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.io.CleanupMode;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS;

public class FunctionalTest {

    @TempDir(cleanup = CleanupMode.ON_SUCCESS)
    private Path projectDir;
    private File settingsFile;
    private File buildFile;

    private File javaFile;

    private static final String javaFileWithIssues = "public class Issues {" +
            "String doStuff(){" +
            "String x = null;" +
            "return x.toLowerCase();" +
            "}" +
            "}";

    private static final String dependencyDebugSnippet = "gradle.taskGraph.whenReady {taskGraph ->\n" +
            "    println \"Found task graph: \" + taskGraph\n" +
            "    println \"Found \" + taskGraph.allTasks.size() + \" tasks.\"\n" +
            "    taskGraph.allTasks.forEach { task ->\n" +
            "        println task\n" +
            "        task.dependsOn.forEach { dep ->\n" +
            "            println \"  - \" + dep\n" +
            "        }\n" +
            "    }\n" +
            "}\n";


    @BeforeEach
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressFBWarnings("RV_RETURN_VALUE_IGNORED_BAD_PRACTICE")
    void setUp() throws IOException {
        settingsFile = projectDir.resolve("settings.gradle").toFile();
        buildFile = projectDir.resolve("build.gradle").toFile();
        javaFile = projectDir.resolve("src/main/java/Issues.java").toFile();

        settingsFile.createNewFile();
        buildFile.createNewFile();
        javaFile.getParentFile().mkdirs();
        javaFile.createNewFile();
    }

    @Test
    public void pluginRunsSuccessfully() throws IOException {
        writeFile(settingsFile, "rootProject.name = 'project-1'");
        String buildFileContent =
                "plugins {\n" +
                "    id 'java'\n" +
                "    id 'com.gitlab.mqus.gitlab-lint-report'\n" +
                "}\n" +
                "\n" +
                "version = 1.0\n" +
                "\n" +
                "repositories {\n" +
                "    mavenCentral()\n" +
                "}";
        writeFile(buildFile, buildFileContent);

        BuildResult result = GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .withArguments("convertLintReportsForGitLab", "--stacktrace")
                .build();

        assertThat(result.task(":convertLintReportsForGitLab").getOutcome()).isEqualTo(SUCCESS);
        assertThat(projectDir.resolve("build/reports/gitlab/quality.json")).isRegularFile();
    }

    @Test
    public void pluginWritesEmptyFileWithoutPlugin() throws IOException {
        writeFile(settingsFile, "rootProject.name = 'project-1'");
        String buildFileContent =
                "plugins {\n" +
                        "    id 'java'\n" +
                        "    id 'com.gitlab.mqus.gitlab-lint-report'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n" +
                        "\n" +
                        "repositories {\n" +
                        "    mavenCentral()\n" +
                        "}";
        writeFile(buildFile, buildFileContent);

        GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .forwardOutput()
                .withArguments("convertLintReportsForGitLab")
                .build();

        assertThat(projectDir.resolve("build/reports/gitlab/quality.json")).content().isEqualTo("[ ]");
    }

    @Test
    public void pluginWritesEmptyFileWithSpotbugsPlugin() throws IOException {
        writeFile(settingsFile, "rootProject.name = 'project-1'");
        String buildFileContent =
                "plugins {\n" +
                        "    id 'java'\n" +
                        "    id 'com.gitlab.mqus.gitlab-lint-report'\n" +
                        "    id 'com.github.spotbugs' version '4.0.8'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n" +
                        "\n" +
                        "repositories {\n" +
                        "    mavenCentral()\n" +
                        "}";
        writeFile(buildFile, buildFileContent);

        GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .withArguments("spotbugsMain", "convertLintReportsForGitLab")
                .build();

        assertThat(projectDir.resolve("build/reports/gitlab/quality.json")).content().isEqualTo("[ ]");
    }
    @Test
    public void pluginWritesFileWithSpotbugsPluginAndIssuesFile() throws IOException {
        writeFile(settingsFile, "rootProject.name = 'project-1'");
        String buildFileContent =
                "plugins {\n" +
                        "    id 'java'\n" +
                        "    id 'com.gitlab.mqus.gitlab-lint-report'\n" +
                        "    id 'com.github.spotbugs' version '4.0.8'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n" +
                        "\n" +
                        "spotbugs {\n" +
                        "  ignoreFailures = true\n" +
                        "}\n" +
                        "repositories {\n" +
                        "    mavenCentral()\n" +
                        "}\n"
                +dependencyDebugSnippet;
        writeFile(buildFile, buildFileContent);
        writeFile(javaFile, javaFileWithIssues);


        GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .forwardOutput()
                .withArguments("spotbugsMain", "convertLintReportsForGitLab", "--stacktrace")
                .build();

        assertThat(projectDir.resolve("build/reports/gitlab/quality.json")).content()
                .isNotEqualTo("[ ]")
                .contains("src/main/java/Issues.java");
    }

    @Test
    public void pluginWritesFileWithSpotbugsPluginAndIssuesFile_differentSpotbugsReportDir() throws IOException {
        writeFile(settingsFile, "rootProject.name = 'project-1'");
        String buildFileContent =
                "plugins {\n" +
                        "    id 'java'\n" +
                        "    id 'com.gitlab.mqus.gitlab-lint-report'\n" +
                        "    id 'com.github.spotbugs' version '4.0.8'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n" +
                        "\n" +
                        "spotbugs {\n" +
                        "  ignoreFailures = true\n" +
                        "  reportsDir = file(\"$buildDir/notspotbugs\")\n" +
                        "}\n" +
                        "repositories {\n" +
                        "    mavenCentral()\n" +
                        "}\n"
                +dependencyDebugSnippet;
        writeFile(buildFile, buildFileContent);
        writeFile(javaFile, javaFileWithIssues);


        GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .forwardOutput()
                .withArguments("convertLintReportsForGitLab", "spotbugsMain", "--stacktrace")
                .build();

        assertThat(projectDir.resolve("build/reports/gitlab/quality.json")).content()
                .isNotEqualTo("[ ]")
                .contains("src/main/java/Issues.java");
    }

    @Test
    public void pluginWritesFileWithPMDPluginAndIssuesFile() throws IOException {
        writeFile(settingsFile, "rootProject.name = 'project-1'");
        String buildFileContent =
                "plugins {\n" +
                        "    id 'java'\n" +
                        "    id 'com.gitlab.mqus.gitlab-lint-report'\n" +
                        "    id 'pmd'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n" +
                        "\n" +
                        "tasks.withType(VerificationTask.class).configureEach {\n" +
                        "    ignoreFailures = true\n" +
                        "}\n" +
                        "repositories {\n" +
                        "    mavenCentral()\n" +
                        "}\n"
                +dependencyDebugSnippet;
        writeFile(buildFile, buildFileContent);
        writeFile(javaFile, javaFileWithIssues);


        GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .forwardOutput()
                .withArguments("pmdMain", "convertLintReportsForGitLab", "--stacktrace")
                .build();

        assertThat(projectDir.resolve("build/reports/gitlab/quality.json")).content()
                .isNotEqualTo("[ ]")
                .contains("src/main/java/Issues.java");
    }
    @Test
    public void pluginDoesNotRecomputeWithoutChanges() throws IOException {
        writeFile(settingsFile, "rootProject.name = 'project-1'");
        String buildFileContent =
                "plugins {\n" +
                        "    id 'java'\n" +
                        "    id 'com.gitlab.mqus.gitlab-lint-report'\n" +
                        "    id 'pmd'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n" +
                        "\n" +
                        "tasks.withType(VerificationTask.class).configureEach {\n" +
                        "    ignoreFailures = true\n" +
                        "}\n" +
                        "repositories {\n" +
                        "    mavenCentral()\n" +
                        "}\n"
                        +dependencyDebugSnippet;
        writeFile(buildFile, buildFileContent);
        writeFile(javaFile, javaFileWithIssues);


        GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .withArguments("pmdMain", "convertLintReportsForGitLab")
                .build();

        // run a second time
        BuildResult result = GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .forwardOutput()
                .withArguments("convertLintReportsForGitLab")
                .build();

        assertThat(result.task(":convertLintReportsForGitLab").getOutcome()).isEqualTo(TaskOutcome.UP_TO_DATE);
    }


    private void writeFile(File destination, String content) throws IOException {

        try (BufferedWriter output = new BufferedWriter(new OutputStreamWriter(Files.newOutputStream(destination.toPath()), StandardCharsets.UTF_8))) {
            output.write(content);
        }
    }
}
