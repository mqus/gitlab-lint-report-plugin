import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.gradle.testkit.runner.GradleRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.CleanupMode;
import org.junit.jupiter.api.io.TempDir;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

public class FunctionalMultiProjectTest {

    @TempDir(cleanup = CleanupMode.ON_SUCCESS)
    private Path projectDir;
    private File buildFile1;
    private File buildFile2;

    private File javaFile1, javaFile2;

    private static final String javaFileWithIssues = "public class Issues {" +
            "String doStuff(){" +
            "String x = null;" +
            "return x.toLowerCase();" +
            "}" +
            "}";

    private static final String dependencyDebugSnippet = "gradle.taskGraph.whenReady {taskGraph ->\n" +
            "    println \"Found task graph: \" + taskGraph\n" +
            "    println \"Found \" + taskGraph.allTasks.size() + \" tasks.\"\n" +
            "    taskGraph.allTasks.forEach { task ->\n" +
            "        println task\n" +
            "        task.dependsOn.forEach { dep ->\n" +
            "            println \"  - \" + dep\n" +
            "        }\n" +
            "    }\n" +
            "}\n";


    @SuppressWarnings("ResultOfMethodCallIgnored")
    @BeforeEach
    @SuppressFBWarnings("RV_RETURN_VALUE_IGNORED_BAD_PRACTICE")
    void setUp() throws IOException {
        File settingsFile = projectDir.resolve("settings.gradle").toFile();
        File buildFileRoot = projectDir.resolve("build.gradle").toFile();
        settingsFile.createNewFile();
        buildFileRoot.createNewFile();
        writeFile(settingsFile, "rootProject.name = 'project-1'\n" +
                "include('sub:proj1')\n" +
                "include('sub:proj2')\n"
        );
        String rootBuildFileContent =
                "plugins {\n" +
                        "    id 'com.gitlab.mqus.gitlab-lint-report'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n"
                        +dependencyDebugSnippet;

        writeFile(buildFileRoot, rootBuildFileContent);

        buildFile1 = projectDir.resolve("sub/proj1/build.gradle").toFile();
        javaFile1 = projectDir.resolve("sub/proj1/src/main/java/Issues.java").toFile();
        javaFile1.getParentFile().mkdirs();
        javaFile1.createNewFile();
        buildFile1.createNewFile();

        buildFile2 = projectDir.resolve("sub/proj2/build.gradle").toFile();
        javaFile2 = projectDir.resolve("sub/proj2/src/main/java/Issues.java").toFile();
        javaFile2.getParentFile().mkdirs();
        javaFile2.createNewFile();
        buildFile2.createNewFile();
    }

    @Test
    public void pluginWritesFileWithSpotbugsPluginAndIssuesFile() throws IOException {
        String buildFileContent =
                "plugins {\n" +
                        "    id 'java'\n" +
                        "    id 'com.github.spotbugs'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n" +
                        "\n" +
                        "spotbugs {\n" +
                        "  ignoreFailures = true\n" +
                        "}\n" +
                        "repositories {\n" +
                        "    mavenCentral()\n" +
                        "}\n";
        writeFile(buildFile1, buildFileContent);
        writeFile(buildFile2, buildFileContent);
        writeFile(javaFile1, javaFileWithIssues);
        writeFile(javaFile2, javaFileWithIssues);


        GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .forwardOutput()
                .withArguments("spotbugsMain", "convertLintReportsForGitLab", "--stacktrace")
                .build();

        assertThat(projectDir.resolve("build/reports/gitlab/quality.json")).content()
                .isNotEqualTo("[ ]")
                .contains("sub/proj1/src/main/java/Issues.java")
                .contains("sub/proj2/src/main/java/Issues.java");
    }


    @Test
    public void pluginWritesFileWithPMDPluginAndIssuesFile() throws IOException {
        String buildFileContent =
                "plugins {\n" +
                        "    id 'java'\n" +
                        "    id 'pmd'\n" +
                        "}\n" +
                        "\n" +
                        "version = 1.0\n" +
                        "\n" +
                        "tasks.withType(VerificationTask.class).configureEach {\n" +
                        "    ignoreFailures = true\n" +
                        "}\n" +
                        "repositories {\n" +
                        "    mavenCentral()\n" +
                        "}\n";
        writeFile(buildFile1, buildFileContent);
        writeFile(buildFile2, buildFileContent);
        writeFile(javaFile1, javaFileWithIssues);
        writeFile(javaFile2, javaFileWithIssues);


        GradleRunner.create()
                .withProjectDir(projectDir.toFile())
                .withPluginClasspath()
                .forwardOutput()
                .withArguments("pmdMain", "convertLintReportsForGitLab", "--stacktrace")
                .build();

        assertThat(projectDir.resolve("build/reports/gitlab/quality.json")).content()
                .isNotEqualTo("[ ]")
                .contains("sub/proj1/src/main/java/Issues.java")
                .contains("sub/proj2/src/main/java/Issues.java");
    }


    private void writeFile(File destination, String content) throws IOException {

        try (BufferedWriter output = new BufferedWriter(new OutputStreamWriter(Files.newOutputStream(destination.toPath()), StandardCharsets.UTF_8))) {
            output.write(content);
        }
    }
}
