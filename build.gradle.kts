plugins {
    `java-gradle-plugin`
    id("com.github.spotbugs") version "4.0.8"
}

group = "com.gitlab.mqus"
version = "1.0-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://plugins.gradle.org/m2/")
    }
}

dependencies {
    implementation("com.fasterxml.jackson.core:jackson-annotations:2.9.1")
    implementation("com.fasterxml.jackson.core:jackson-core:2.9.1")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.9.1")

    implementation("gradle.plugin.com.github.spotbugs.snom:spotbugs-gradle-plugin:4.0.8")

    compileOnly("com.github.spotbugs:spotbugs-annotations:4.8.0")
    testCompileOnly("com.github.spotbugs:spotbugs-annotations:4.8.0")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.assertj:assertj-core:3.24.2")
//    testImplementation(gradleTestKit())
}

tasks.test {
    useJUnitPlatform()
}

//testing {
//    suites.create<JvmTestSuite>("intTest")
//}

gradlePlugin {
    plugins {
        create("simplePlugin") {
            id = "com.gitlab.mqus.gitlab-lint-report"
            implementationClass = "com.gitlab.mqus.gitlab.LintReportPlugin"
        }
    }
}