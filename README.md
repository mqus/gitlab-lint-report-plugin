# Gitlab lint report

This project is not officially associated with GitLab!

A Gradle plugin to convert your linter reports (e.g. Spotbugs or PMD xml output)
to the [json format](https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#data-types) that gitlab wants to have (no CodeQuality step necessary)

[Documentation in GitLab](https://docs.gitlab.com/ee/ci/testing/code_quality.html)

## Using it

1. Apply the plugin to the project (or root project in multi-project-builds), like specified on the plugin page in gradle

Currently, all generated reports of the following tools are taken and included in the output file:
- Spotbugs
- PMD

This plugin automatically determines the locations of those paths, even if they get relocated to custom locations.
The only requirement is that xml reports get generated. Other ones will not be parsed.

2. Add it to your `.gitlab-ci.yml`:
```yml
#TODO
```

### Minimum supported versions:

- Gradle: 5.6.4
- Java Runtime when using gradle: 8
- Spotbugs-Gradle-Plugin: 4.0.8

## TODO
- test cacheability (building incrementally is tested already, not sure about the gradle build cache though)
- test various (newer) versions properly
- create branch for java 17+/gradle 7+ stuff
- add logger(?)